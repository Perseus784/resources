# README #

### What is this repository for? ###
List of anything useful.

# Projects:
* [Google Bulk Image download](https://github.com/hardikvasa/google-images-download)  
* [Data Augmentation](https://github.com/mdbloice/Augmentor)

# Websites:
* [Unofficial Python Libraries](http://www.lfd.uci.edu/~gohlke/pythonlibs/)
* [ML datasets](http://archive.ics.uci.edu/ml/datasets.html)
* [Indian Govt datasets](https://data.gov.in/)
# Commands:
* [Python Tricks](http://sahandsaba.com/thirty-python-language-features-and-tricks-you-may-not-know.html)

